const coursesData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
        price: 55000,
        onOffer: true
    },
    {
        id: "wdc004",
        name: "Javascript",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
        price: 4000,
        onOffer: true
    },
]

//export default para mareuse ung code na to
export default coursesData;