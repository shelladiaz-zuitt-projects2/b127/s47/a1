import { Fragment, useEffect, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register(){

//useState. state hooks to store the values of the input fields
const [email, setEmail] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
const [isActive, setIsActive] = useState(false);

console.log(email);
console.log(password1);
console.log(password2);

useEffect(()=>{
    //validation to enable submit button when all fields are populated and both passwords match
    if((email !== '' && password1 !== '' && password2 !== '')&& (password1===password2)){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [email, password1, password2])

    function registerUser(e){
        e.preventDefault();
        //to clear out the data in our input fields
        setEmail('')
        setPassword1('')
        setPassword2('')
        
        Swal.fire({
            title: 'Yaaaaaaaaaaaaaaaay!',
            icon: 'success',
            text: 'Thank you for registering!'
        })
    }

    return(
        <Fragment>
            <h1>Register</h1>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group>
                    <Form.Label>
                        Email address:
                    </Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter Email"
                        value={email}
                        //the e.target.value property allows us to gain access to the input fields current value to be used when submitting form data
                        onChange={e => setEmail(e.target.value)}
                        required

                    />
                    <Form.Text className="text-muted">
                        We will never share your email with everyone else.
                    </Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter your Password"
                        onChange={e => setPassword1(e.target.value)}
                        value={password1}
                        required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Verify your Password"
                        onChange={e => setPassword2(e.target.value)}
                        value={password2}
                        required
                    />
                </Form.Group>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }
       
               
            </Form>
        </Fragment>
        
    )
}