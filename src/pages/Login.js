import { Fragment, useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
//reactcontext
import UserContext from '../UserContext';

export default function Login(){

    //userContext is a react hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.PRovider component in App.js)
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);

    useEffect(()=>{
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])

    function loginUser(e){
        e.preventDefault();
     
        Swal.fire({
            title: 'Yaaaaaaaaaaaaaaaay!',
            icon: 'success',
            text: 'Welcome!'
        })
        //local storage allows us to save data within our browsers as strings
        //the setItem() method of the storage interface, when passed a key name and value, will add that key to the given storage object or update the key's value if its already exists
        //setItem is used to store data in the localStorage as a string
        localStorage.setItem('email', email)
        setUser({ email: email })

        setEmail('')
        setPassword('')
        
    }

    return(
        
        <Fragment>
            <h1>Login</h1>

            <Form onSubmit={(e) => loginUser(e)}>
            
                <Form.Group>
                <Form.Label>
                        Email address:
                    </Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required

                    />

                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter your Password"
                        onChange={e => setPassword(e.target.value)}
                        value={password}
                        required
                    />
                </Form.Group>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }

            </Form>

        </Fragment>


    )

    
}