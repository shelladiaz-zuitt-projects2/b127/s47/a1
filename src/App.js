import  './App.css';
//import react component if madami na ung return
//Ayaw gumana ng Fragment kaya React ang ginamit ko
import React from 'react';
//import navbar
import AppNavbar from './components/AppNavbar';
//import home
import Home from './pages/Home';
//import courses
import Courses from './pages/Courses';
// import Counter from './components/Counter';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
//import React Context
import UserContext from './UserContext';

//routing components
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { Fragment, useState } from 'react';


// import container from bootsrap
import { Container } from 'react-bootstrap';



function App() {

  //add a state hook for user
  //the getItem() method returns value of the specified storage object item
  const [user, setUser] = useState({email: localStorage.getItem('email')})

  return (
    //provider components that allows consuming components to subscribe to context changes
    <UserContext.Provider value={ {user, setUser} }>
      <Router>
        <AppNavbar/>
        <Container>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/courses" component={Courses} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              {/* <Counter /> */}
              <Route component={Error}/>
            </Switch>
        </Container>
      </Router>
    </UserContext.Provider>
    

  );
}

export default App;
